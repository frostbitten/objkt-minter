import {
  TezosToolkit,
  ContractAbstraction,
  Wallet,
  MichelsonMap,
  OpKind,
  ContractProvider
} from "@taquito/taquito";
import { BeaconWallet } from "@taquito/beacon-wallet";
import { char2Bytes, buf2hex, hex2buf } from "@taquito/utils";
import { RequestSignPayloadInput, SigningType } from "@airgap/beacon-sdk";
import { get } from "svelte/store";
import blake from "blakejs";
// import { TestSettings, TestResult } from "./types";
import store from "./store";
import localStore from "./store";
import axios from 'axios';

export let initializeCollections = async (
  Tezos: TezosToolkit,
  userAddress: string,
  wallet: BeaconWallet | undefined
): Promise<ArrayBuffer[]> => {
	let collections;
	await axios //find collection ids created by user
		.get('https://api.tzkt.io/v1/bigmaps/24157/keys?value.creator='+userAddress)
		.then(res => {
			// console.log('collection response:', res)
			collections = res.data;
		})
	for (let [i,collection] of collections.entries()){ // load data for each collection
		let metadata_id, cid_raw;
		await axios // get bigmap id of collection metadata
			.get('https://api.tzkt.io/v1/contracts/'+collection.value.contract+'/storage?path=metadata')
			.then(res => {
				console.log('metadata_id response:', res)
				metadata_id = res.data;
			})
		await axios // get metadata cid
		.get('https://api.tzkt.io/v1/bigmaps/'+metadata_id+'/keys')
		.then(res => {
			console.log('collection metadata_info response:', res)
			const metadata_info = res.data;
			const metadata_cid = Buffer.from(metadata_info[0].value, 'hex').toString()
			collection.metadata_cid = metadata_cid
			cid_raw = metadata_cid.split('ipfs://')[1]
		})
		await axios // load metadata from ipfs via cid
		.get('https://ipfs.io/ipfs/'+cid_raw)
		.then(res => {
			console.log('collection metadata response:', res)
			collection.metadata = res.data;
		})
	}
	console.log('collections:', collections)
	return collections;
};
