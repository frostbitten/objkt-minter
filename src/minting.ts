import {
  TezosToolkit,
  ContractAbstraction,
  Wallet,
  MichelsonMap,
  OpKind,
  ContractProvider
} from "@taquito/taquito";
import { BeaconWallet } from "@taquito/beacon-wallet";
import { char2Bytes, buf2hex, hex2buf } from "@taquito/utils";
import { RequestSignPayloadInput, SigningType } from "@airgap/beacon-sdk";
import { get } from "svelte/store";
import blake from "blakejs";
// import { TestSettings, TestResult } from "./types";
import store from "./store";
import localStore from "./store";


export const additionalAssetsConfig = {
	cover: {
		quality: .85,
		maxWidth: 1024,
		maxHeight: 1024
	},
	thumbnail: {
		quality: .85,
		maxWidth: 350,
		maxHeight: 350
	}
}

export async function thumbnail(file, tool,settings_key){
	return new Promise(async (resolve,reject)=>{
		const settings = additionalAssetsConfig[settings_key];
		const name_base = file.name.substring(0, file.name.lastIndexOf('.'));
		const thumbData = await tool.thumbnail(settings.maxWidth).quality(settings.quality);
		const blob = await thumbData.toBlob();
		// console.log('blob type:',blob.type)
		const thumb = await thumbData.toFile(settings_key+'-'+name_base+'.jpg');
		const url = URL.createObjectURL(thumb);
		let _returnData = {
			file: thumb,
			url: url,
			dimensions: {
				width: null,
				height: null,
			},
			type: blob.type
		}
		const img = new Image;
		img.onload = function() {
			_returnData.dimensions.width = img.width;
			_returnData.dimensions.height = img.height;
			resolve(_returnData)
		};
		img.src = url;
	})
}
	
export const notEmptyPattern = "[^ 	]+"
export const ipfsPattern = "ipfs://Qm[1-9A-HJ-NP-Za-km-z]{44,}|b[A-Za-z2-7]{58,}|B[A-Z2-7]{58,}|z[1-9A-HJ-NP-Za-km-z]{48,}|F[0-9A-F]{50,}"
export const walletPattern = "^(tz1|tz2|tz3|KT1)[0-9a-zA-Z]{33}";
export const mimeTypes = ["image/jpeg","image/png","image/gif","image/webp","image/flif","image/x-xcf","image/x-canon-cr2","image/x-canon-cr3","image/tiff","image/bmp","image/vnd.ms-photo","image/vnd.adobe.photoshop","application/x-indesign","application/epub+zip","application/x-xpinstall","application/vnd.oasis.opendocument.text","application/vnd.oasis.opendocument.spreadsheet","application/vnd.oasis.opendocument.presentation","application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/zip","application/x-tar","application/x-rar-compressed","application/gzip","application/x-bzip2","application/x-7z-compressed","application/x-apple-diskimage","application/x-apache-arrow","video/mp4","audio/midi","video/x-matroska","video/webm","video/quicktime","video/vnd.avi","audio/vnd.wave","audio/qcelp","audio/x-ms-asf","video/x-ms-asf","application/vnd.ms-asf","video/mpeg","video/3gpp","audio/mpeg","audio/mp4","audio/opus","video/ogg","audio/ogg","application/ogg","audio/x-flac","audio/ape","audio/wavpack","audio/amr","application/pdf","application/x-msdownload","application/x-shockwave-flash","application/rtf","application/wasm","font/woff","font/woff2","application/vnd.ms-fontobject","font/ttf","font/otf","image/x-icon","video/x-flv","application/postscript","application/eps","application/x-xz","application/x-sqlite3","application/x-nintendo-nes-rom","application/x-google-chrome-extension","application/vnd.ms-cab-compressed","application/x-deb","application/x-unix-archive","application/x-rpm","application/x-compress","application/x-lzip","application/x-cfb","application/x-mie","application/mxf","video/mp2t","application/x-blender","image/bpg","image/jp2","image/jpx","image/jpm","image/mj2","audio/aiff","application/xml","application/x-mobipocket-ebook","image/heif","image/heif-sequence","image/heic","image/heic-sequence","image/icns","image/ktx","application/dicom","audio/x-musepack","text/calendar","text/vcard","model/gltf-binary","application/vnd.tcpdump.pcap","audio/x-dsf","application/x.ms.shortcut","application/x.apple.alias","audio/x-voc","audio/vnd.dolby.dd-raw","audio/x-m4a","image/apng","image/x-olympus-orf","image/x-sony-arw","image/x-adobe-dng","image/x-nikon-nef","image/x-panasonic-rw2","image/x-fujifilm-raf","video/x-m4v","video/3gpp2","application/x-esri-shape","audio/aac","audio/x-it","audio/x-s3m","audio/x-xm","video/MP1S","video/MP2P","application/vnd.sketchup.skp","image/avif","application/x-lzh-compressed","application/pgp-encrypted","application/x-asar","model/stl","application/vnd.ms-htmlhelp","model/3mf","image/jxl","application/zstd"]

// export null;

export const mintDataSchema = {
	type: "object",
	properties: {
		name: {
			type: "string",
			pattern: notEmptyPattern,
			"minLength": 1,
		},
		description: {
			type: "string",
			pattern: notEmptyPattern,
			"minLength": 1,
		},
		rights: {
			type: "string"
		},
		minter: {
			type: "string",
			pattern: walletPattern
		},
		tags: {
			type: "array",
			items: {
				type: "string"
			}
		},
		symbol: {
			type: "string"
		},
		artifactUri: {
			type: "string",
			pattern: ipfsPattern
		},
		displayUri: {
			type: "string",
			pattern: ipfsPattern
		},
		thumbnailUri: {
			type: "string",
			pattern: ipfsPattern
		},
		creators: {
			type: "array",
			items: {
				type: "string",
				pattern: walletPattern
			}
		},
		contributors: {
			type: "array",
			items: {
				type: "string",
				pattern: walletPattern
			}
		},
		date: {
			type: "string",
			pattern: "^\\d{4}-\\d\\d-\\d\\dT\\d\\d:\\d\\d:\\d\\d(\\.\\d+)?(([+-]\\d\\d:\\d\\d)|Z)?"
		},
		formats: {
			type: "array",
			items: {
				type: "object",
				properties: {
					uri: {
						type: "string",
						pattern: ipfsPattern
					},
					mimeType: {
						type: "string",
						enum: mimeTypes
					},
					fileSize: {
						type: "number",
						minimum: 0
					},
					fileName: {
						type: "string"
					},
					dimensions: {
						type: "object",
						properties: {
							value: {
								type: "string",
								pattern: "^(\\d*\\.?\\d+)\\s*x\\s*(\\d*\\.?\\d+)"
							},
							unit: {
								type: "string"
							}
						},
						required: ["value", "unit"],
						additionalProperties: !0
					}
				},
				required: ["uri", "mimeType"],
				additionalProperties: !0
			}
		},
		attributes: {
			type: "array",
			items: {
				type: "object",
				properties: {
					name: {
						type: "string"
					},
					value: {
						type: "string"
					},
					type: {
						type: "string"
					},
					trait_type: {
						type: "string"
					}
				},
				required: ["value"],
				additionalProperties: !0
			}
		},
		decimals: {
			type: "number",
			minimum: 0
		},
		isBooleanAmount: {
			type: "boolean"
		},
		shouldPreferSymbol: {
			type: "boolean"
		},
		royalties: {
			type: "object",
			decimals: {
				type: "number",
				minimum: 0
			},
			shares: {
				type: "object",
				additionalProperties: !0
			}
		}
	},
	required: ["name", "description", "tags", "symbol", "artifactUri", "displayUri", "thumbnailUri", "creators", "formats", "decimals", "isBooleanAmount", "shouldPreferSymbol"],
	additionalProperties: !1
}



// const mintMetaDataTemplate { 
 /*  "name": "test",
  "description": "test",
  "rights": "No License / All Rights Reserved",
  "minter": "KT1Aq4wWmVanpQhq4TTfjZXB5AjFpx15iQMM",
  "date": "2021-11-18T06:58:46.997Z",
  "tags": [],
  "symbol": "OBJKTCOM",
  "artifactUri": "ipfs://Qmb2UWYTWHG8QXEF4DWLgbAUzZUCjvoX5oczVA7i2cSG6K",
  "displayUri": "ipfs://QmVXuxD3fHaVFTpEP5jx7N9rBP83oDywvorbH7iYVzu4D8",
  "thumbnailUri": "ipfs://QmNZ4RmwkwvnG27iQ9GxAwymSMhVVWo5nGPFhP7ACSQHPM",
  "image": "ipfs://QmVXuxD3fHaVFTpEP5jx7N9rBP83oDywvorbH7iYVzu4D8",
  "creators": [
    "tz1NoYMQaZa9Pz6Hwfx6B2x1TaGU3fSiBbW8"
  ],
  "formats": [
    {
      "uri": "ipfs://Qmb2UWYTWHG8QXEF4DWLgbAUzZUCjvoX5oczVA7i2cSG6K",
      "mimeType": "image/png",
      "fileSize": 11920808,
      "fileName": "stylestyle-pixabay-roses-66527_1920_x_publicdomainpictures-grafiti-wall-background-1473148216RmG-ms720-i450.png",
      "dimensions": {
        "value": "2880x2880",
        "unit": "px"
      }
    },
    {
      "uri": "ipfs://QmVXuxD3fHaVFTpEP5jx7N9rBP83oDywvorbH7iYVzu4D8",
      "mimeType": "image/jpeg",
      "fileName": "cover-stylestyle-pixabay-roses-66527_1920_x_publicdomainpictures-grafiti-wall-background-1473148216RmG-ms720-i450.jpg",
      "fileSize": 668608,
      "dimensions": {
        "value": "1024x1024",
        "unit": "px"
      }
    },
    {
      "uri": "ipfs://QmNZ4RmwkwvnG27iQ9GxAwymSMhVVWo5nGPFhP7ACSQHPM",
      "mimeType": "image/jpeg",
      "fileName": "thumbnail-stylestyle-pixabay-roses-66527_1920_x_publicdomainpictures-grafiti-wall-background-1473148216RmG-ms720-i450.jpg",
      "fileSize": 128392,
      "dimensions": {
        "value": "350x350",
        "unit": "px"
      }
    }
  ],
  "attributes": [],
  "decimals": 0,
  "isBooleanAmount": false,
  "shouldPreferSymbol": false,
  "royalties": {
    "decimals": 3,
    "shares": {
      "tz1NoYMQaZa9Pz6Hwfx6B2x1TaGU3fSiBbW8": 100
    }
  } */
// }

/* generateCompressedImage = async(e,t,n)=>{
	// ES.a.debug("generateCompressedImage");
	const r = new Uint8Array(t)
	  , i = {
		name: e.name,
		content: r
	};
	let o = [];
	o = e.type === CS.k.GIF ? ["convert", e.name, "-verbose", "-coalesce", "-resize", `${n.maxWidth}x${n.maxHeight}>`, "-deconstruct", "out.gif"] : ["convert", e.name, "-verbose", "-resize", `${n.maxWidth}x${n.maxHeight}>`, "out.jpg"];
	const s = await Object(IS.call)([i], o);
	// if (ES.a.debug(s),
	// 0 !== s.exitCode)
		// return ES.a.error(s.stderr.join("\n")),
		null;
	if (s.outputFiles.length > 0) {
		let e = s.outputFiles[0].blob;
		const t = CS.k.JPEG
		  , n = await e.arrayBuffer()
		  , r = await (async e=>(ES.a.debug("blobToDataURL"),
		new Promise((t,n)=>{
			let r = new FileReader;
			r.onerror = n,
			r.onload = e=>t(r.result),
			r.readAsDataURL(e)
		}
		)))(e);
		let i = 0
		  , o = 0;
		if (s.stdout && s.stdout.length > 0) {
			let e, t = 0;
			for (; null !== (e = MS.exec(s.stdout[0])); )
				1 === t && (i = e[1],
				o = e[2]),
				t++
		}
		return {
			mimeType: t,
			buffer: n,
			reader: r,
			size: n.byteLength,
			width: i,
			height: o
		}
	}
	return ES.a.debug("outputFiles.length == 0"),
	null
 */
// }

/* 

	var vS = n("mrSG")
	  , _S = n("z6cu")
	  , wS = n("XH0y")
	  , ES = n.n(wS)
	  , CS = n("KmPd")
	  , IS = n("d+Ov");
	const MS = /(\d*\.?\d+)\s*x\s*(\d*\.?\d+)/g
	  , generateCompressedImage = async(e,t,n)=>{
		ES.a.debug("generateCompressedImage");
	}
	  , generateCoverAndThumbnail = async(e,t)=>{
		let n, r;
		return ES.a.debug("generateCoverAndThumbnail"),
		n = await generateCompressedImage(e, t, CS.a),
		n && (r = await generateCompressedImage(e, t, CS.r)),
		{
			cover: n,
			thumbnail: r
		}
	}
	;
	var kS = n("un/a")
	  , TS = n("cQK7")
	  , BS = n("eaQS");
	const PS = ["fileElement"]
	  , OS = ["coverElement"];
	function DS(e, t) {
		1 & e && i.Ub(0, "div", 2)
	}
	function LS(e, t) {
		1 & e && (i.Zb(0, "p"),
		i.Mc(1, "File is required"),
		i.Yb())
	}
	function RS(e, t) {
		if (1 & e && (i.Zb(0, "div", 38),
		i.Kc(1, LS, 2, 0, "p", 39),
		i.Yb()),
		2 & e) {
			const e = i.kc(3);
			i.Hb(1),
			i.qc("ngIf", e.tokenFileMissing)
		}
	}
	function jS(e, t) {
		1 & e && (i.Zb(0, "div", 38),
		i.Zb(1, "p"),
		i.Mc(2, "File is required"),
		i.Yb(),
		i.Yb())
	}
	function NS(e, t) {
		if (1 & e) {
			const e = i.ac();
			i.Zb(0, "div", 40),
			i.Zb(1, "label", 10),
			i.Mc(2, "Upload Cover "),
			i.Zb(3, "p", 11),
			i.Mc(4, "Types supported: gif, jpeg, png. Max file size is 2MB"),
			i.Yb(),
			i.Kc(5, jS, 3, 0, "div", 12),
			i.Yb(),
			i.Zb(6, "app-upload", 41),
			i.gc("fileChange", function(t) {
				return i.Dc(e),
				i.kc(3).onChangeCover(t)
			})("fileRemove", function() {
				return i.Dc(e),
				i.kc(3).onRemoveCover()
			}),
			i.Yb(),
			i.Yb()
		}
		if (2 & e) {
			const e = i.kc(3);
			i.Hb(5),
			i.qc("ngIf", e.coverFileMissing),
			i.Hb(1),
			i.Lb("invalid", e.coverFileMissing),
			i.qc("maxSizeMB", 2)
		}
	}
	function QS(e, t) {
		1 & e && (i.Zb(0, "p"),
		i.Mc(1, " Title is required. "),
		i.Yb())
	}
	function FS(e, t) {
		1 & e && (i.Zb(0, "p"),
		i.Mc(1, " Title cannot be longer than 200 characters "),
		i.Yb())
	}
	function YS(e, t) {
		if (1 & e && (i.Zb(0, "div", 38),
		i.Kc(1, QS, 2, 0, "p", 39),
		i.Kc(2, FS, 2, 0, "p", 39),
		i.Yb()),
		2 & e) {
			const e = i.kc(3);
			i.Hb(1),
			i.qc("ngIf", e.title.errors.required),
			i.Hb(1),
			i.qc("ngIf", e.title.errors.maxlength)
		}
	}
	function HS(e, t) {
		1 & e && (i.Zb(0, "p"),
		i.Mc(1, " Description is required. "),
		i.Yb())
	}
	function US(e, t) {
		1 & e && (i.Zb(0, "p"),
		i.Mc(1, " Description cannot be longer than 5,000 characters "),
		i.Yb())
	}
	function GS(e, t) {
		if (1 & e && (i.Zb(0, "div", 38),
		i.Kc(1, HS, 2, 0, "p", 39),
		i.Kc(2, US, 2, 0, "p", 39),
		i.Yb()),
		2 & e) {
			const e = i.kc(3);
			i.Hb(1),
			i.qc("ngIf", e.description.errors.required),
			i.Hb(1),
			i.qc("ngIf", e.description.errors.maxlength)
		}
	}
	function zS(e, t) {
		if (1 & e && (i.Zb(0, "option", 42),
		i.Mc(1),
		i.Yb()),
		2 & e) {
			const e = t.$implicit
			  , n = t.index;
			i.qc("value", e.collection_id)("selected", 0 === n),
			i.Hb(1),
			i.Oc(" ", e.name, " ")
		}
	}
	function qS(e, t) {
		if (1 & e && (i.Zb(0, "option", 43),
		i.Mc(1),
		i.Yb()),
		2 & e) {
			const e = t.$implicit;
			i.qc("value", e),
			i.Hb(1),
			i.Oc(" ", e, " ")
		}
	}
	function VS(e, t) {
		1 & e && (i.Zb(0, "p"),
		i.Mc(1, " Editions is required. "),
		i.Yb())
	}
	function WS(e, t) {
		1 & e && (i.Zb(0, "p"),
		i.Mc(1, " Editions cannot be larger than 10,000 "),
		i.Yb())
	}
	function KS(e, t) {
		1 & e && (i.Zb(0, "p"),
		i.Mc(1, " Editions must be larger than 0 "),
		i.Yb())
	}
	function ZS(e, t) {
		if (1 & e && (i.Zb(0, "div", 38),
		i.Kc(1, VS, 2, 0, "p", 39),
		i.Kc(2, WS, 2, 0, "p", 39),
		i.Kc(3, KS, 2, 0, "p", 39),
		i.Yb()),
		2 & e) {
			const e = i.kc(3);
			i.Hb(1),
			i.qc("ngIf", e.editions.errors.required),
			i.Hb(1),
			i.qc("ngIf", e.editions.errors.max),
			i.Hb(1),
			i.qc("ngIf", e.editions.errors.min)
		}
	}
	function JS(e, t) {
		1 & e && (i.Zb(0, "p"),
		i.Mc(1, " Royalty is required. "),
		i.Yb())
	}
	function XS(e, t) {
		1 & e && (i.Zb(0, "p"),
		i.Mc(1, " Royalty cannot be larger than 25% "),
		i.Yb())
	}
	function $S(e, t) {
		1 & e && (i.Zb(0, "p"),
		i.Mc(1, " Editions must be at least 0% "),
		i.Yb())
	}
	function ex(e, t) {
		if (1 & e && (i.Zb(0, "div", 38),
		i.Kc(1, JS, 2, 0, "p", 39),
		i.Kc(2, XS, 2, 0, "p", 39),
		i.Kc(3, $S, 2, 0, "p", 39),
		i.Yb()),
		2 & e) {
			const e = i.kc(3);
			i.Hb(1),
			i.qc("ngIf", e.royalty.errors.required),
			i.Hb(1),
			i.qc("ngIf", e.royalty.errors.max),
			i.Hb(1),
			i.qc("ngIf", e.royalty.errors.min)
		}
	}
	function tx(e, t) {
		if (1 & e) {
			const e = i.ac();
			i.Zb(0, "button", 44),
			i.gc("click", function() {
				return i.Dc(e),
				i.kc(3).mint()
			}),
			i.Mc(1, "Mint"),
			i.Yb()
		}
	}
	function nx(e, t) {
		1 & e && (i.Zb(0, "p", 45),
		i.Zb(1, "small"),
		i.Mc(2, "Preparing file..."),
		i.Yb(),
		i.Yb(),
		i.Ub(3, "div", 46))
	}
	function rx(e, t) {
		if (1 & e && i.Ub(0, "app-art-view", 47),
		2 & e) {
			const e = i.kc(3);
			i.qc("objkt", e.objkt)
		}
	}
	function ix(e, t) {
		if (1 & e && i.Ub(0, "div", 48),
		2 & e) {
			const e = i.kc(3);
			i.Lb("gradient-loading", e.coverLoading)
		}
	} */