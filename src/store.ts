import { writable } from "svelte/store";


interface State {
  collections: any[] | undefined;
  userAddress: string | undefined;
  confirmationObservableTest:
    | undefined
    | { level: number; currentConfirmation: number }[];
}

const initialState: State = {
  collections: [],
  userAddress: undefined,
  confirmationObservableTest: undefined
};

const store = writable(initialState);

const state = {
  subscribe: store.subscribe,
  
  updateCollections: (collections: any) =>
    store.update(store => ({
      ...store,
      collections: collections
    })),
  updateUserAddress: (address: string) =>
    store.update(store => ({
      ...store,
      userAddress: address
    })),
};

export default state;
